{
    "id": "e8b97996-b2af-493f-8302-c1137bcaecbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSolid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "929a37ed-7812-414c-9c2c-fa4d717a35ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8b97996-b2af-493f-8302-c1137bcaecbe",
            "compositeImage": {
                "id": "d778f969-bb62-4a88-858a-6339cdac1e6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929a37ed-7812-414c-9c2c-fa4d717a35ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cb46a8f-d6ce-4477-b54c-2f1293be1dd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929a37ed-7812-414c-9c2c-fa4d717a35ec",
                    "LayerId": "21580633-1200-41d0-af29-20f65aa0a0b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "21580633-1200-41d0-af29-20f65aa0a0b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8b97996-b2af-493f-8302-c1137bcaecbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}