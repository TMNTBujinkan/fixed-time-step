{
    "id": "6eec7662-f8b5-4ee0-a8c9-1b6a3db70332",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "329b679b-b8a1-400a-9d86-fe1cedfdce53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6eec7662-f8b5-4ee0-a8c9-1b6a3db70332",
            "compositeImage": {
                "id": "93d9aed0-22d0-43d1-b76e-caf6dca02ee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "329b679b-b8a1-400a-9d86-fe1cedfdce53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea72a40-b4cc-4d4b-83de-a71b6e33a5bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "329b679b-b8a1-400a-9d86-fe1cedfdce53",
                    "LayerId": "7b9fc8d1-3f09-4fa4-8dc3-711a15c5e003"
                }
            ]
        },
        {
            "id": "d7cebac8-b074-43d5-a553-29ee7b9f27c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6eec7662-f8b5-4ee0-a8c9-1b6a3db70332",
            "compositeImage": {
                "id": "a12897b3-847b-47cf-9105-e374077be6e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7cebac8-b074-43d5-a553-29ee7b9f27c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad641c3-0368-43c4-9fdb-ca81aa0c391c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7cebac8-b074-43d5-a553-29ee7b9f27c2",
                    "LayerId": "7b9fc8d1-3f09-4fa4-8dc3-711a15c5e003"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7b9fc8d1-3f09-4fa4-8dc3-711a15c5e003",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6eec7662-f8b5-4ee0-a8c9-1b6a3db70332",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}