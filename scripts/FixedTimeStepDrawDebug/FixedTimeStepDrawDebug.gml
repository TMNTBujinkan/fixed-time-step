/// @func FixedTimeStepDrawDebug()
/// @desc Draws list of objects, user events, scripts and type of them on the screen

/// @param x
/// @param y
/// @param padding

var _x			= argument[0];
var _y			= argument[1];
var _padding	= argument[2];

var _space		= 0;
var _item		= 0;
var _objectName	= "";
var _scriptName	= "";


for(var _i = 0; _i < global.tsBeginStepListSize; _i++)
{
	_item		= global.tsBeginStepList[| _i];
	_objectName	= object_get_name(_item[enumTimeStepItem.ObjIndex]);
	_scriptName	= script_get_name(_item[enumTimeStepItem.Script]);
	
	// ObjIndex | UserEvent | Script | Type
	draw_text(_x, _y + _i * _padding, string(_objectName) + " | " + string(_item[enumTimeStepItem.UserEvent]) + " | " + string(_scriptName) + " | [beginStep]");	
}

_space += global.tsBeginStepListSize * _padding;
for(var _i = 0; _i < global.tsStepListSize; _i++)
{
	_item		= global.tsStepList[| _i];
	_objectName	= object_get_name(_item[enumTimeStepItem.ObjIndex]);
	_scriptName	= script_get_name(_item[enumTimeStepItem.Script]);
	
	// ObjIndex | UserEvent | Script | Type
	draw_text(_x, _space + _y + _i * _padding, string(_objectName) + " | " + string(_item[enumTimeStepItem.UserEvent]) + " | " + string(_scriptName) + " | [step]");	
}

_space += global.tsStepListSize * _padding;
for(var _i = 0; _i < global.tsEndStepListSize; _i++)
{
	_item		= global.tsEndStepList[| _i];
	_objectName	= object_get_name(_item[enumTimeStepItem.ObjIndex]);
	_scriptName	= script_get_name(_item[enumTimeStepItem.Script]);
	
	// ObjIndex | UserEvent | Script | Type
	draw_text(_x, _space + _y + _i * _padding, string(_objectName) + " | " + string(_item[enumTimeStepItem.UserEvent]) + " | " + string(_scriptName) + " | [endStep]");
}