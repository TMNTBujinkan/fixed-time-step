/// @desc FixedTimeStepCleanUp()
/// @desc Destroys fixed time step data strcutures

ds_list_destroy(global.tsBeginStepList);
ds_list_destroy(global.tsStepList);
ds_list_destroy(global.tsEndStepList);