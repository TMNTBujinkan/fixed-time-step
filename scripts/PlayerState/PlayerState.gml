/// @desc PlayerState()

#region // Update previous positions

xPrevious = xCurrent;
yPrevious = yCurrent;

#endregion

#region // Update hspd

if(move != 0)
{
	hspd	= Approach(hspd, spd * global.scale * move, acc * global.scale);
}
else
{
	hspd	= Approach(hspd, 0, fric * global.scale);
}

#endregion

#region // Collision method one
	
	#region // Hor collision

	xCurrent += hspd;

	if(place_meeting(xCurrent, yCurrent, oSolid))
	{
		var _solidId = instance_place(xCurrent, yCurrent, oSolid);
	
		if(hspd > 0)
		{
			xCurrent = (_solidId.bbox_left - 1) - spriteBboxRight;	
		}
		else if(hspd < 0)
		{
			xCurrent = (_solidId.bbox_right + 1) - spriteBboxLeft;
		}
	
		hspd = 0;
	}

	#endregion

	#region // Ver collision

	// Gravity
	oldVspd = vspd;
	vspd	= min(vspd + grav * global.scale, maxFallSpeed * global.scale);

	if(place_meeting(xCurrent, yCurrent + 1, oSolid) && vspd >= 0)
	{
		vspd = 0;
		oldVspd = 0;
	}

	yCurrent = yCurrent + (oldVspd + vspd) * 0.5 * global.scale;

	if(place_meeting(xCurrent, yCurrent, oSolid))
	{
		var _solidId = instance_place(xCurrent, yCurrent, oSolid);
	
		if(vspd > 0)
		{
			yCurrent = (_solidId.bbox_top - 1) - spriteBboxBottom;	
		}
		else if(vspd < 0)
		{
			yCurrent = (_solidId.bbox_bottom + 1) - spriteBboxTop;
		}
	
		vspd = 0;	
		oldVspd = 0;
	}
	
	#endregion
	
#endregion

#region // Collision method two
	/*
	#region // Hor collision
	
	if(place_meeting(xCurrent + hspd, yCurrent, oSolid))
	{
		while(!place_meeting(xCurrent + sign(hspd), yCurrent, oSolid))
		{
			xCurrent += sign(hspd);	
		}
	
		hspd = 0;
	}

	xCurrent += hspd;
	
	#endregion

	#region // Ver collision
	
	// Gravity
	oldVspd = vspd;
	vspd	= min(vspd + grav * global.scale, maxFallSpeed * global.scale);

	if(place_meeting(xCurrent, yCurrent + vspd, oSolid))
	{
		while(!place_meeting(xCurrent, yCurrent + sign(vspd), oSolid))
		{
			yCurrent += sign(vspd);	
		}
	
		vspd	= 0;
		oldVspd	= 0;
	}

	yCurrent = yCurrent + (oldVspd + vspd) * 0.5 * global.scale;
	
	#endregion
	*/
#endregion

// Calculate jump height
if(vspd >= 0 && heightEnd == 0 && !place_meeting(xCurrent, yCurrent + 1, oSolid))
{
	heightEnd = yCurrent;
}