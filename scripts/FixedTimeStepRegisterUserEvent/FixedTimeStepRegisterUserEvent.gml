/// @func FixedTimeStepRegisterUserEvent()
/// @desc Registers object type to the fixed update loop. Will execute passed user event in the scope of instance on fixed tick

/// @param objectIndex
/// @param userEventNumber
/// @param eventType [BeginStep, Step, EndStep]

var _objectIndex		= argument[0];
var _userEventNumber	= argument[1];
var _eventType			= argument[2];

global.array[enumTimeStepItem.ObjIndex]		= _objectIndex;
global.array[enumTimeStepItem.UserEvent]	= _userEventNumber;
global.array[enumTimeStepItem.Script]		= -1;

switch(_eventType)
{
	case BeginStep:
	{
		ds_list_add(global.tsBeginStepList, global.array);

		global.tsBeginStepListSize += 1;
	
		break;
	}
	
	case Step:
	{
		ds_list_add(global.tsStepList, global.array);

		global.tsStepListSize += 1;
		
		break;
	}
	
	case EndStep:
	{
		ds_list_add(global.tsEndStepList, global.array);

		global.tsEndStepListSize += 1;
		
		break;
	}
}

