/// @func FixedTimeStepRegisterScript()
/// @desc Registers object type to the fixed update loop. Will execute passed script in the scope of instance on fixed tick

/// @param objectIndex
/// @param scriptName
/// @param eventType [BeginStep, Step, EndStep]

var _objectIndex	= argument[0];
var _scriptName		= argument[1];
var _eventType		= argument[2];

global.array[enumTimeStepItem.ObjIndex]		= _objectIndex;
global.array[enumTimeStepItem.UserEvent]	= -1;
global.array[enumTimeStepItem.Script]		= _scriptName;

switch(_eventType)
{
	case BeginStep:
	{
		ds_list_add(global.tsBeginStepList, global.array);

		global.tsBeginStepListSize += 1;
	
		break;
	}
	
	case Step:
	{
		ds_list_add(global.tsStepList, global.array);

		global.tsStepListSize += 1;
		
		break;
	}
	
	case EndStep:
	{
		ds_list_add(global.tsEndStepList, global.array);

		global.tsEndStepListSize += 1;
		
		break;
	}
}

