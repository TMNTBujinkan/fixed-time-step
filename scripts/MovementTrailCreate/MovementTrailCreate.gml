/// @func MovementTrailCreate()

/// @param trailWidth
/// @param trailColor
/// @param trailAlpha
/// @param trailMaxLength
/// @param trailMinDistanceToAddPoint


var _width					= argument[0];
var _color					= argument[1];
var _alpha					= argument[2]
var _maxLength				= argument[3];
var _minDistanceToAddPoint	= argument[4];

trailWidth					= _width;
trailWidthHalf				= trailWidth * 0.5;
trailColor					= _color;
trailAlpha					= _alpha;
trailMaxLength				= _maxLength;
trailMinDistanceToAddPoint	= _minDistanceToAddPoint;
canDrawMovementTrail		= false;

trailList = ds_list_create();

vertex_format_begin();
vertex_format_add_position();
vertex_format_add_colour();
vertexFormat = vertex_format_end();

vertexBuffer = vertex_create_buffer();