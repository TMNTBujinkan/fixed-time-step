/// @func FixedTimeStepCreate()
/// @desc Creates global variables for fixed time step

#macro BeginStep	0
#macro Step			1
#macro EndStep		2

enum enumTimeStepItem
{
	ObjIndex,	// 0
	UserEvent,	// 1
	Script,		// 2
	Number		// 3
}

global.tsAccumulator		= 0;
global.tsRenderAlpha		= 1;

global.tsBeginStepList		= ds_list_create(); 
global.tsStepList			= ds_list_create(); 
global.tsEndStepList		= ds_list_create(); 
global.array				= array_create(enumTimeStepItem.Number, 0);

global.tsBeginStepListSize	= 0;
global.tsStepListSize		= 0;
global.tsEndStepListSize	= 0;

global.tsDelta				= delta_time / 1000000; 
global.tsMinFPS				= 5; 
global.tsTargetFPS			= 60;
global.tsDeltaRestored		= false;
global.tsStepsTaken			= 0; 
global.tsDisabled			= false; 
global.tsInterpolation		= false;

