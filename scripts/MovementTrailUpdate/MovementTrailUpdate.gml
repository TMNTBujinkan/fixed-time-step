/// @func MovementTrailUpdate()

#region // Fill the trail ds list

var _disToPreviousPosition = point_distance(xPrevious, yPrevious, xCurrent, yCurrent);

if(_disToPreviousPosition >= trailMinDistanceToAddPoint)
{
	ds_list_add(trailList, xCurrent, yCurrent);	
}

var _trailListSize = ds_list_size(trailList);

if(_trailListSize > trailMaxLength * 2)
{
	repeat(2)	
	{
		ds_list_delete(trailList, 0);
	}
}

#endregion

#region // Fill the vertex buffer

vertex_begin(vertexBuffer, vertexFormat);

var _currentX, _currentY, _nextX, _nextY, _direction, _lengthDirX, _lengthDirY;
_trailListSize = ds_list_size(trailList);

for(var _i = 0; _i < _trailListSize; _i += 2)
{
	_currentX	= trailList[| _i];
	_currentY	= trailList[| _i + 1];
	
	if(_i == _trailListSize - 2)
	{
		_nextX	= _currentX;
		_nextY	= _currentY;
	}
	else
	{
		_nextX	= trailList[| _i + 2];
		_nextY	= trailList[| _i + 3];			
	}
	
	_direction	= point_direction(_currentX, _currentY, _nextX, _nextY) + 90;
	_lengthDirX	= lengthdir_x(trailWidthHalf, _direction);
	_lengthDirY	= lengthdir_y(trailWidthHalf, _direction);
	
	vertex_position(vertexBuffer, _currentX + _lengthDirX, _currentY + _lengthDirY);
	vertex_color(vertexBuffer, trailColor, trailAlpha);
	
	vertex_position(vertexBuffer, _currentX - _lengthDirX, _currentY - _lengthDirY);
	vertex_color(vertexBuffer, trailColor, trailAlpha);
}

vertex_end(vertexBuffer);

canDrawMovementTrail = true;

#endregion
