/// @func MovementTrailDestroy()

ds_list_destroy(trailList);
vertex_format_delete(vertexFormat);
vertex_delete_buffer(vertexBuffer);