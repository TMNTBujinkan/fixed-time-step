/// @func FixedTimeStepUpdate()
/// @desc Executes object scripts and user events

if(global.tsDisabled)
{
	exit;	
}

var _nextTimeStep	= delta_time / 1000000;
var _minTimeStep	= 1 / global.tsMinFPS;

if(_nextTimeStep > _minTimeStep)
{
	if(global.tsDeltaRestored)
	{
		global.tsDelta = _minTimeStep;
	}
	
	global.tsDeltaRestored = true;
}

global.tsDeltaRestored	= false; 
global.tsDelta			= _nextTimeStep;

var _delta		= global.tsDelta;
var _deltaMax	= 1 / global.tsTargetFPS;
var _stepsTaken	= 0;
var _item		= 0;

global.tsAccumulator	+= _delta;
global.tsStepsTaken		= 0;

while(global.tsAccumulator >= _deltaMax)
{
	global.tsAccumulator -= _deltaMax;
	
	#region // Execute user events and scripts in begin step
	
	for(var _beginStepCounter = 0; _beginStepCounter < global.tsBeginStepListSize; ++_beginStepCounter)
	{
		_item = global.tsBeginStepList[| _beginStepCounter];
		
		if(_item[enumTimeStepItem.UserEvent] != -1)
		{
			with(_item[enumTimeStepItem.ObjIndex])
			{
				event_user(_item[enumTimeStepItem.UserEvent])	
			}
		}
		else if(_item[enumTimeStepItem.Script] != -1)
		{
			with(_item[enumTimeStepItem.ObjIndex])
			{
				script_execute(_item[enumTimeStepItem.Script]);
			}
		}
	}
	
	#endregion
	
	#region // Execute user events and scripts in step
	
	for(var _stepCounter = 0; _stepCounter < global.tsStepListSize; ++_stepCounter)
	{
		_item = global.tsStepList[| _stepCounter];
		
		if(_item[enumTimeStepItem.UserEvent] != -1)
		{
			with(_item[enumTimeStepItem.ObjIndex])
			{
				event_user(_item[enumTimeStepItem.UserEvent])	
			}
		}
		else if(_item[enumTimeStepItem.Script] != -1)
		{
			with(_item[enumTimeStepItem.ObjIndex])
			{
				script_execute(_item[enumTimeStepItem.Script]);
			}
		}
	}
	
	#endregion
	
	#region // Execute user events and scripts in end step
	
	for(var _endStepCounter = 0; _endStepCounter < global.tsEndStepListSize; ++_endStepCounter)
	{
		_item = global.tsEndStepList[| _endStepCounter];
		
		if(_item[enumTimeStepItem.UserEvent] != -1)
		{
			with(_item[enumTimeStepItem.ObjIndex])
			{
				event_user(_item[enumTimeStepItem.UserEvent])	
			}
		}
		else if(_item[enumTimeStepItem.Script] != -1)
		{
			with(_item[enumTimeStepItem.ObjIndex])
			{
				script_execute(_item[enumTimeStepItem.Script]);
			}
		}
	}
	
	#endregion
	
	++_stepsTaken;
	
	#region // Pause game again

	if(global.frameByFrame)
	{
		global.frameByFrame	= false;
		global.tsDisabled	= true;	
	
		exit;
	}

	#endregion
}

// Calculate blended alpha
global.tsRenderAlpha	= global.tsAccumulator / _deltaMax;
global.tsStepsTaken		= _stepsTaken;
