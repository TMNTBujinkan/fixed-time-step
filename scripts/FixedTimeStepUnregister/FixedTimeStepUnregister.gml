/// @func FixedTimeStepUnregister()
/// @desc Removes object from the update list

/// @param objectIndex
/// @param eventType

var _objectIndex	= argument[0];
var _eventType		= argument[1];

var _item			= 0;

switch(_eventType)
{
	case BeginStep:
	{
		for(var _i = 0; _i < global.tsBeginStepListSize; _i++)
		{
			_item = global.tsBeginStepList[| _i];

			if(_item[enumTimeStepItem.ObjIndex] == _objectIndex)
			{
				ds_list_delete(global.tsBeginStepList, _i);
				global.tsBeginStepListSize -= 1;
			}
		}
			
		break;
	}
	
	case Step:
	{
		for(var _i = 0; _i < global.tsStepListSize; _i++)
		{
			_item = global.tsStepList[| _i];

			if(_item[enumTimeStepItem.ObjIndex] == _objectIndex)
			{
				ds_list_delete(global.tsStepList, _i);
				global.tsStepListSize -= 1;
			}
		}
			
		break;
	}
	
	case EndStep:
	{
		for(var _i = 0; _i < global.tsEndStepListSize; _i++)
		{
			_item = global.tsEndStepList[| _i];

			if(_item[enumTimeStepItem.ObjIndex] == _objectIndex)
			{
				ds_list_delete(global.tsEndStepList, _i);
				global.tsEndStepListSize -= 1;
			}
		}
			
		break;
	}
}


