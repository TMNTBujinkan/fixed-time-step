/// @description Insert description here
// You can write your code in this editor

MovementTrailDraw();

draw_set_halign(fa_center);
draw_set_valign(fa_middle);

draw_text(xCurrent, yCurrent - 100, "vspd       : " + string_format(vspd, 1, 2));

var _jumpHeight = heightStart - heightEnd;
_jumpHeight = (_jumpHeight >= 0 && vspd >= 0) ? _jumpHeight : 0;
draw_text(xCurrent, yCurrent - 120, "jumpHeight : " + string_format(_jumpHeight, 1, 2));

if(global.tsInterpolation)
{
	draw_sprite_ext(sprite_index, image_index, lerp(xPrevious, xCurrent, global.tsRenderAlpha), lerp(yPrevious, yCurrent, global.tsRenderAlpha), image_xscale, image_yscale, image_angle, image_blend, image_alpha);
}
else
{
	draw_sprite(sprite_index, image_index, xCurrent, yCurrent);
}

