/// @description Insert description here
// You can write your code in this editor

#region // Simple timer 

if(time == 0)
{
	time = global.currentTime;	
}

if(global.currentTime >= time + timer)
{
	show_debug_message(global.currentTime - (time + timer));
	
	time		= 0;
	image_index = (image_index == 0) ? 1 : 0;
}

#endregion