
FixedTimeStepCreate();

global.scale		= 1;
global.scaleTraget	= 0.5;
global.frameByFrame	= false;
global.deltaTime	= 0;
global.currentTime	= 0;


global.deltaTime = delta_time / 1000;

var _result = (1000 / global.tsMinFPS);
if(global.deltaTime > _result)
{
	global.deltaTime = _result;
}

global.currentTime += global.deltaTime * global.scale;
