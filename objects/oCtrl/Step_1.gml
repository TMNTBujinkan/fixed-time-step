
#region // Change game FPS

if(keyboard_check_pressed(vk_f1))
{
	var _gameSpeed = game_get_speed(gamespeed_fps);
	
	switch(_gameSpeed)
	{
		case 5:
		{
			game_set_speed(10, gamespeed_fps);
			break;
		}
		
		case 10:
		{
			game_set_speed(15, gamespeed_fps);
			break;
		}
		
		case 15:
		{
			game_set_speed(30, gamespeed_fps);
			break;
		}
		
		case 30:
		{
			game_set_speed(45, gamespeed_fps);
			break;
		}
		
		case 45:
		{
			game_set_speed(60, gamespeed_fps);
			break;
		}
		
		case 60:
		{
			game_set_speed(75, gamespeed_fps);
			break;
		}
		
		case 75:
		{
			game_set_speed(90, gamespeed_fps);
			break;
		}
		
		case 90:
		{
			game_set_speed(120, gamespeed_fps);
			break;
		}
		
		case 120:
		{
			game_set_speed(144, gamespeed_fps);
			break;
		}
		
		case 144:
		{
			game_set_speed(240, gamespeed_fps);
			break;
		}
		
		case 240:
		{
			game_set_speed(9999, gamespeed_fps);
			break;
		}
		
		case 9999:
		{
			game_set_speed(5, gamespeed_fps);
			break;
		}
	}
}

#endregion

#region // Disable fixed update

global.tsDisabled ^= keyboard_check_pressed(vk_f2)

#endregion

#region // Toggle frame by frame test

// If game is in the pause mode
if(global.tsDisabled)
{
	global.frameByFrame ^= keyboard_check_pressed(ord("F")); 
	
	// Unpause game for one farme
	if(global.frameByFrame)
	{
		global.tsDisabled = false;
	}
}

#endregion

#region // Toggle interpolation

global.tsInterpolation ^= keyboard_check_pressed(vk_f3); 

#endregion

// Set game speed scale
if(keyboard_check_pressed(vk_shift))
{
	global.scale = (global.scale == 1) ? global.scaleTraget : 1;	
}

// Restart the game
if(keyboard_check_pressed(ord("R")))
{
	game_restart();	
}

// Exit game
if(keyboard_check_pressed(vk_escape))
{
	game_end();
}
