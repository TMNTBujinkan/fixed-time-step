
draw_set_halign(fa_left);
draw_set_valign(fa_middle);

FixedTimeStepDrawDebug(20, 20, 20);

draw_text(550, 165,
	"Press A and D to walk around. \n" +
	"Press space to jump. \n" +
	"Press shift to toggle slow motion. \n" +
	"Press F1 to change FPS. \n" +
	"Press F2 to toggle disable fixed timestep. \n" +
	"Press F to toggle farme by frame test in pause mode. \n" +
	"Press F3 to toggle interpolation. \n" +
	"Press R to restart the game. \n" +
	"Press Escape to quit the game. \n" +
	"Render FPS     : " + string(room_speed) + "\n" +
	"Logic FPS      : " + string(global.tsTargetFPS) + "\n" +
	"Fixed Timestep : " + string((global.tsDisabled == true) ? "False" : "True") + "\n" +
	"Interpolation  : " + string((global.tsInterpolation == true) ? "On" : "Off") + "\n" + 
	"Game scale     : " + string(global.scale) + "\n" + 
	"Current time   : " + string(global.currentTime)
);



